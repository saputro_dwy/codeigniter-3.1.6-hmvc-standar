<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function currency_rupiah($jumlah){
    return 'Rp. ' . number_format( $jumlah, 0 , '' , '.' ) . ',-';
}

function get_uuid() {
    $CI =& get_instance();
    $CI->load->model('mgeneral');

    return $CI->mgeneral->get_uuid();
}

function get_next_id($table, $primary_key) {
    $CI =& get_instance();
    $CI->load->model('mgeneral');

    return $CI->mgeneral->get_next_id($table, $primary_key);
}

function insert($table, $data) {
    $CI =& get_instance();
    $CI->load->model('mgeneral');

    return $CI->mgeneral->insert($table, $data);
}

function update($table, $data, $where) {
    $CI =& get_instance();
    $CI->load->model('mgeneral');

    return $CI->mgeneral->update($table, $data, $where);
}

function get_client_ip()
{
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if (isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if (isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function do_upload($folder, $var="file", $preffix="_", $replace_name=nul) {
	$allowed='png,jpg,gif,pdf,doc,docx,xls,xlsx';
	$extension_allowed=explode(',', $allowed);

	if(isset($_FILES[$var])){
		$result = array();
		if(is_array($_FILES[$var]['name'])) {
            foreach ($_FILES[$var]['name'] as $key => $value) {
                if ($value == "") continue;
                $file_name = $_FILES[$var]['name'][$key];
				$extension = strtolower(end((explode('.', $_FILES[$var]['name']))));

                $new_file_name = $preffix . $file_name;
				if($replace_name!=null)
					$new_file_name = $replace_name.".".$extension;
                $file_directory_saved = "assets/upload/" . $folder;

                if (!file_exists($file_directory_saved)) {
                    mkdir($file_directory_saved, 0777, true);
                }
				if(in_array($extension, $extension_allowed)) {
	                if(move_uploaded_file($_FILES[$var]['tmp_name'][$key], $file_directory_saved . "/" . $new_file_name)){
	                	array_push($result, array(
	                        "file_loc" => "upload/" . $folder . "/" . $new_file_name,
	                        "file_size" => $_FILES[$var]['size'][$key],
	                        "file_name" => $new_file_name,
	                        "file_type" => $_FILES[$var]['type'][$key],
	                    ));
	            	}
	            }
        	}
     	}else{
            if ($_FILES[$var]['name'] == "")
                return $result;
            $file_name = $_FILES[$var]['name'];
			$extension = strtolower(end((explode('.', $_FILES[$var]['name']))));

            $new_file_name = $preffix . $file_name;
			if($replace_name!=null)
				$new_file_name = $replace_name.".".$extension;
            $file_directory_saved = "assets/upload/" . $folder;

            if (!file_exists($file_directory_saved)) {
                mkdir($file_directory_saved, 0777, true);
            }
			if(in_array($extension, $extension_allowed)) {
	            if(move_uploaded_file($_FILES[$var]['tmp_name'], $file_directory_saved . "/" . $new_file_name)){
		            array_push($result, array(
		                    "file_loc" => "upload/" . $folder . "/" . $new_file_name,
		                    "file_size" => $_FILES[$var]['size'],
		                    "file_name" => $new_file_name,
		                    "file_type" => $_FILES[$var]['type'],
		                )
		            );
				}
			}
        }

		return $result;
	}
	return false;
}

function do_send_email($email, $alias, $subject, $html, $plaintext, $attach=null){
    $CI =& get_instance();
    $CI->load->library('load_phpmailer');
    $mail = new PHPMailer;
    $mail->isSMTP();
    $mail->SMTPDebug = 0;
    $mail->Debugoutput = 'html';

    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = "username@gmail.com";
    $mail->Password = "**password**";

    $mail->setFrom('username@gmail.com', 'Alias Name');
    $mail->addAddress($email, $alias);

    $mail->isHTML(true);
    $mail->Subject = $subject;
    $mail->Body = $html;
    $mail->AltBody = $plaintext;
    //Attach an image file
    if($attach)
        foreach ($attach as $key => $value) {
            $mail->addAttachment($value);
        }

    if (!$mail->send()) return false;
    else  return true;
}


//Export Excel
function do_init_excel($nama_file){
    $CI =& get_instance();
    $CI->load->library('load_phpexcel');
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator("Creator")
        ->setLastModifiedBy("Creator")
        ->setTitle($nama_file)
        ->setSubject("Subject")
        ->setDescription($nama_file)
        ->setKeywords($nama_file)
        ->setCategory($nama_file);

    return $objPHPExcel;
}

function do_write_excel($objPHPExcel, $nama_file, $type){
    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    if($type==TYPE_EXCEL_2007) {
        $type_writer = 'Excel2007';
        $type_file = '.xlsx';
        $content_type = 'Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
    }else{
        $type_writer = 'Excel5';
        $type_file = '.xls';
        $content_type = 'Content-Type: application/vnd.ms-excel';
    }
    // Redirect output to a client’s web browser (Excel2007)
    header($content_type);
    header('Content-Disposition: attachment;filename="'.$nama_file.$type_file);
    header('Cache-Control: max-age=0');
    // If you're serving to IE 9, then the following may be needed
    header('Cache-Control: max-age=1');

    // If you're serving to IE over SSL, then the following may be needed
    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
    header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
    header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
    header ('Pragma: public'); // HTTP/1.0

    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, $type_writer);

    if (!$objWriter->save('php://output')) return false;
    else  return true;
}