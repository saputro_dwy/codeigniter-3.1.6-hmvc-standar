<?php 
class Mgeneral extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function get_uuid (){
        $sql = "select uuid() as id";
        return $this->db->query($sql)->row()->id;
    }

    function get_next_id($nama_table, $primary_key){
        $sql = "SELECT concat('" . date("Ym") . "',RIGHT(concat( '000000' , CAST(IFNULL(MAX(CAST(right($primary_key,6) AS 
			unsigned)), 0) + 1 AS unsigned)),6)) as `data` 
			FROM $nama_table";
        $sql .= " WHERE LEFT($primary_key,6) = '" . date("Ym") . "' ";
        $dt = $this->db->query($sql)->row();
        $strresult = $dt->data;
        return $strresult;
    }

    function insert ($nama_table, $data){
        $this->db->insert($nama_table, $data);
    }

    function update ($nama_table, $data, $where){
        $this->db->update($nama_table, $data, $where);
    }
}

?>